package com.example.userservice.service;

import com.example.userservice.model.ActivityType;
import com.example.userservice.model.User;
import com.example.userservice.model.UserActivity;
import com.example.userservice.repository.UserRepository;
import com.example.userservice.utils.ElasticUtils;
import com.google.common.collect.Lists;
import org.elasticsearch.index.query.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static com.example.userservice.utils.TaskUtils.simulate3rdPartyUserProcessing;
import static java.util.concurrent.CompletableFuture.supplyAsync;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ElasticsearchOperations esRestTemplate;

    public User findById(String id) {
        return userRepository.findById(id).orElse(null);
    }

    public List<User> findByActivityType(String activityType) {
        QueryBuilder activityTypeFilter = ElasticUtils.getActivityTypeFilter(activityType);
        SearchQuery searchQuery = new NativeSearchQueryBuilder().withQuery(activityTypeFilter).build();
        return esRestTemplate.queryForList(searchQuery, User.class);
    }

    public List<User> getAllUsers() {
        return Lists.newArrayList(userRepository.findAll());
    }

    public void add(User user) {
        if (user.getUserId() == null) {
            //just for testing purposes
            user.setUserId(UUID.randomUUID().toString());
        }
        ActivityType activityType = ActivityType.USER_CREATED;
        user.addUserActivity(new UserActivity(activityType, activityType.generateMessage(String.valueOf(user.getUserId()))));
        userRepository.save(user);
    }

    public void update(User user) {
        userRepository.save(user);
    }

    public void updateLongRunning(User user) {
        CompletableFuture<User> completableFuture = supplyAsync(
                //simulate submitting a task to a 3-rd party service
                () -> simulate3rdPartyUserProcessing(user))
                .whenComplete((updatedUser, e) -> update(updatedUser));
    }

    public void update(Map<String, Object> changes, String id) {
        Optional<User> user = userRepository.findById(id);
        user.ifPresent(toUpdate -> {
            changes.forEach(
                    (change, value) -> {
                        switch (change) {
                            case "username":
                                ActivityType activityType = ActivityType.USERNAME_UPDATED;
                                toUpdate.addUserActivity(new UserActivity(activityType,
                                        activityType.generateMessage(String.valueOf(id), toUpdate.getUsername(), (String) value)));
                                toUpdate.setUsername((String) value);
                                break;
                            case "password":
                                toUpdate.setPassword((String) value);
                                break;
                            case "email":
                                activityType = ActivityType.EMAIL_UPDATED;
                                toUpdate.addUserActivity(new UserActivity(activityType,
                                        activityType.generateMessage(String.valueOf(id), toUpdate.getEmail(), (String) value)));
                                toUpdate.setEmail((String) value);
                                break;
                            case "firstName":
                                toUpdate.setFirstName((String) value);
                                break;
                            case "lastName":
                                toUpdate.setLastName((String) value);
                                break;
                            case "deleted":
                                toUpdate.setDeleted((Boolean) value);
                                break;
                        }
                    }
            );
            userRepository.save(toUpdate);
        });
    }

    public void deleteById(String id) {
        User user = findById(id);
        ActivityType activityType = ActivityType.USER_DELETED;
        user.addUserActivity(new UserActivity(activityType, activityType.generateMessage(String.valueOf(user.getUserId()))));
        user.setDeleted(true);
        userRepository.save(user);
    }

}
