package com.example.userservice.utils;

import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.index.query.QueryBuilder;

import static com.example.userservice.constant.ElasticFields.*;
import static org.elasticsearch.index.query.QueryBuilders.*;

public class ElasticUtils {

    public static QueryBuilder getActivityTypeFilter(String activityType) {
        return nestedQuery(USER_ACTIVITIES, boolQuery()
                .should(matchQuery(USER_ACTIVITIES + FIELD_DELIMITER + ACTIVITY_TYPE, activityType)), ScoreMode.None);
    }
}
