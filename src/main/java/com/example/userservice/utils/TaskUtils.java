package com.example.userservice.utils;

import com.example.userservice.model.User;

public class TaskUtils {

    public static User simulate3rdPartyUserProcessing(User user) {
        //the task will either complete quickly or will take one minute to process
        return Math.random() < 0.5 ? simulateATask(60000, user) : simulateATask(100, user);
    }

    public static User simulateATask(int mills, User user) {
        try {
            Thread.sleep(mills);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return user;
    }


}
