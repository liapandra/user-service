package com.example.userservice.repository;

import com.example.userservice.model.User;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface UserRepository  extends ElasticsearchRepository<User, String> {

}
