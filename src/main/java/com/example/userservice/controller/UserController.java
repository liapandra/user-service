package com.example.userservice.controller;

import com.example.userservice.model.User;
import com.example.userservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/users")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping()
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping(value = "/{id}")
    public User getUser(@PathVariable String id) {
        return userService.findById(id);
    }

    @GetMapping(value = "/activity/{activityType}")
    public List<User> getUsersByActivityType(@PathVariable String activityType) {
        return userService.findByActivityType(activityType);
    }

    @PostMapping()
    public String addUser(@RequestBody User user) {
        userService.add(user);
        return "User is successfully added! UserID:" + user.getUserId();
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> updateUser(@RequestBody User user, @PathVariable String id) {
        userService.update(user);
        return ResponseEntity.ok("resource saved");
    }

    @PutMapping(value = "/{id}/longRunning")
    public ResponseEntity<?> updateUserLongRunning(@RequestBody User user, @PathVariable String id) {
        userService.updateLongRunning(user);
        return ResponseEntity.ok("resource saved");
    }

    @PatchMapping(value = "/{id}")
    public ResponseEntity<?> updateUser(@RequestBody Map<String, Object> changes, @PathVariable String id) {
        userService.update(changes, id);
        return ResponseEntity.ok("resource updated");
    }

    @DeleteMapping(value = "/{id}")
    public String deleteUser(@PathVariable String id) {
        userService.deleteById(id);
        return "User is successfully deleted!";
    }

}
