package com.example.userservice.model;

public enum ActivityType {

    USER_CREATED {
        @Override
        public String generateMessage(String... args) {
            return String.format("User %s is created", args[0]);
        }
    },
    USERNAME_UPDATED {
        @Override
        public String generateMessage(String... args) {
            return String.format("Updated username for user with id %s. Old value: %s, new value: %s", args[0], args[1], args[2]);
        }
    },
    EMAIL_UPDATED {
        @Override
        public String generateMessage(String... args) {
            return String.format("Updated email for user with id %s. Old value: %s, new value: %s", args[0], args[1], args[2]);
        }
    },
    USER_DELETED {
        @Override
        public String generateMessage(String... args) {
            return String.format("User %s is marked as deleted", args[0]);
        }
    };

    public abstract String generateMessage(String... args);


    @Override
    public String toString() {
        return this.name().toLowerCase();
    }
}
