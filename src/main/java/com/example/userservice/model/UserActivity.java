package com.example.userservice.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.Objects;

public class UserActivity {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserActivity.class);
    private ActivityType activityType;
    private String comment;
    private String timestamp;

    public UserActivity(ActivityType activityType, String comment) {
        this.activityType = activityType;
        this.comment = comment;
        this.timestamp = new Timestamp(System.currentTimeMillis()).toString();
        LOGGER.info(comment);
    }

    public UserActivity() {
    }

    public ActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserActivity that = (UserActivity) o;
        return activityType == that.activityType &&
                Objects.equals(comment, that.comment) &&
                Objects.equals(timestamp, that.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(activityType, comment, timestamp);
    }

    @Override
    public String toString() {
        return "UserActivity{" +
                "activityType=" + activityType +
                ", comment='" + comment + '\'' +
                ", timestamp='" + timestamp + '\'' +
                '}';
    }
}
