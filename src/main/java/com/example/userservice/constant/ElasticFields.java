package com.example.userservice.constant;

public class ElasticFields {

    public static final String USER_ACTIVITIES = "userActivities";
    public static final String ACTIVITY_TYPE = "activityType";
    public static final String FIELD_DELIMITER = ".";

}
