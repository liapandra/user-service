package com.example.userservice;

import com.example.userservice.model.ActivityType;
import com.example.userservice.model.User;
import com.example.userservice.model.UserActivity;
import com.example.userservice.utils.ElasticUtils;
import com.google.common.collect.Lists;
import org.elasticsearch.index.query.QueryBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {Config.class})
public class UserServiceIntegrationTests {
    public static final String TEST_INDEX = "test-user-service";

    @Autowired
    private ElasticsearchOperations operations;

    @Test
    public void nestedFilteringByActivityType() {
        List<IndexQuery> indexQueries = getTestQueries();
        prepareIndices();
        operations.bulkIndex(indexQueries);
        operations.refresh(TEST_INDEX);

        QueryBuilder activityTypeFilter = ElasticUtils.getActivityTypeFilter(ActivityType.USER_CREATED.toString());
        SearchQuery searchQuery = new NativeSearchQueryBuilder().withQuery(activityTypeFilter).withIndices(TEST_INDEX).build();
        List<User> users = operations.queryForList(searchQuery, User.class);
        assertThat(users).hasSize(2);
    }

    private void prepareIndices() {
        if (operations.indexExists(TEST_INDEX)) {
            operations.deleteIndex(TEST_INDEX);
        }
        operations.createIndex(TEST_INDEX);
        operations.putMapping(TEST_INDEX, "user", User.class);
    }

    private List<IndexQuery> getTestQueries() {

        User user1 = new User("id1", "j.smith", "12345", "j.smith@gmail.com", "John", "Smith",
                Lists.newArrayList(new UserActivity(ActivityType.USER_CREATED, "User with id id1 created")));

        IndexQuery indexQuery1 = new IndexQuery();
        indexQuery1.setId(String.valueOf(user1.getUserId()));
        indexQuery1.setObject(user1);
        indexQuery1.setIndexName(TEST_INDEX);

        User user2 = new User("id2", "o.hrytsak", "98765", "o.hrytsak@gmail.com", "Olena", "Hrytsak",
                Lists.newArrayList(new UserActivity(ActivityType.USER_CREATED, "User with id id2 created")));

        IndexQuery indexQuery2 = new IndexQuery();
        indexQuery2.setId(String.valueOf(user2.getUserId()));
        indexQuery2.setObject(user2);
        indexQuery2.setIndexName(TEST_INDEX);

        List<IndexQuery> indexQueries = new ArrayList<>();
        indexQueries.add(indexQuery1);
        indexQueries.add(indexQuery2);
        return indexQueries;
    }

}
