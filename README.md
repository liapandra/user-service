## A simple RESTful service for interacting with users.
### Allows having a log history of changes, ability to handle long updates asynchronously.

Elasticsearch 6.8.10 is used. For quicker setup, use:
1. docker pull docker.elastic.co/elasticsearch/elasticsearch:6.8.10

2. a) for starting dev ES:
docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:6.8.10

2. b) for starting qa ES:
docker run -p 9500:9200 -p 9600:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:6.8.10